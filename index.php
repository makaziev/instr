<?php

// ini_set('display_errors',1);
// error_reporting(E_ALL);

require_once 'config/init.php';

/*---------------------------------------------------------*/
$row_4 = 'prof_row.php'; // создать еще один row для профессиональных инструментов, html код оставить тот же.

$main_menu_result = main_menu();
$prof_categories_menu_result = prof_categories_menu();


if (!empty($_GET['brand'])) {
	$brand = $_GET['brand'];
}

if (!empty($_GET['prof'])) {
	$prof = $_GET['prof'];
}

if (!empty($_GET['brandname'])) {
	$brandname = $_GET['brandname'];
}

if (!empty($_GET['category'])) {
	$category = $_GET['category'];
}
$brandname = strtolower($brandname);

if (!empty($_GET['page'])) {
	$pageno = $_GET['page'];
}
if (!isset($pageno)) {
	$pageno = 1;
}

// $second_d = 100;
$first_d = 0;
$second_d = 48;
$prev = '←';
$next = '→';
$_404 = 'Страница не найдена!';
// $prof_brands_arr = ['metabo', 'eibenstock', 'husqvarna', 'knipex', 'rubi'];
// $prof_brands_colors = ['#0C463A', '#FFFFFF', '#FFFFFF', '#ED1B24', '#000000'];
//$get_products = products_by_category($_GET['category'], 0, 100);


// только категория
if (isset($_GET['category']) && empty($brand) && empty($_GET['product'])) {
	if($prof) {
		if($count_of_data = ceil(count_of_data_prof(get_menu_tree($_GET['category']), $brandname)->fetch_row()[0] / 48)) {
			$get_products = products_by_category_prof(get_menu_tree($_GET['category']), $brandname, $first_d, $second_d);
			$row_4 = 'prof_row.php';
		}
		else {
			$_404 = 'Товаров не найдено!';
			$row_4 = '404.php';
		}
	}else {
		if($count_of_data = ceil(count_of_data($_GET['category'])->fetch_row()[0] / 48)) {
			$get_products = products_by_category($_GET['category'], $first_d, $second_d);
			$row_4 = 'row.php';
		}
	}
	// if (products_by_category($_GET['category'], $first_d, $second_d)->fetch_row()) {
	// 	$get_products = products_by_category($_GET['category'], $first_d, $second_d);
	// }

	if (is_numeric($pageno) && $pageno > 0 && $pageno <= $count_of_data) {
		// $first_d = ($pageno - 1) * 100;
		$first_d = ($pageno - 1) * 48;
		$prev_n = $pageno - 1;
		$next_n = $pageno + 1;
		if ($pageno > 1 && $prof) {
			$get_products = products_by_category_prof(get_menu_tree($_GET['category']), $brandname, $first_d, $second_d);
			// echo $_GET['category'] . '<br>';
			// echo $brandname . '<br>';
			// echo $first_d . '<br>';
			// echo $second_d . '<br>';
		}
		elseif ($pageno > 1) {
			$get_products = products_by_category($_GET['category'], $first_d, $second_d);
		}

		if ($pageno == 1) {
			$prev = '';
			$prev_n = '';
		}
		elseif ($pageno == $count_of_data) {
			$next = '';
			$next_n = '';
		}
		if ($count_of_data == 1) {
			$next = '';
			$next_n = '';
		}
	}
}
// только определенный товар
elseif(isset($brand) && isset($_GET['product']) && !isset($_GET['category'])) {
	$row_4 = 'product.php';
	$brand = change_vendor_name($brand);
	if(get_product($brand, $_GET['product'])->fetch_row()) {
		$get_product_result = get_product($brand, $_GET['product'])->fetch_assoc();
	}
}
// Только профессиональный инструмент
elseif(isset($brandname) && isset($_GET['product']) && isset($prof) && !isset($_GET['category'])) {
	$row_4 = 'prof_product.php';
	if(get_product_prof($brandname, $_GET['product'])->fetch_row()) {
		$get_product_result = get_product_prof($brandname, $_GET['product'])->fetch_assoc();
	}
	else {
		$row_4 = '404.php';
	}
}
else {
	$get_products = products_uniq_prof();// products_uniq_list();
	// $row_4 = "404.php";
}

/*---------------------------------------------------------*/

counter();




/*--------------------------------------------------------------------------------*/
$meta_desc = 'мир инструмента грозный, магазин инструментов грозный, магазин инструмента, инструменты грозный, алмазное бурение грозный, электроинструмент грозный, резка бетона грозный, плиткорезы грозный, набор инструмента грозный, metabo грозный, метабо грозный, husqvarna грозный, хускварна грозный, knipex грозный, книпекс грозный, eibenstock грозный, эбеншток грозный,';

$meta_key = 'мир инструмента грозный, магазин инструментов грозный, магазин инструмента, инструменты грозный, алмазное бурение грозный, электроинструмент грозный, резка бетона грозный, плиткорезы грозный, набор инструмента грозный, metabo грозный, метабо грозный, husqvarna грозный, хускварна грозный, knipex грозный, книпекс грозный, eibenstock грозный, эбеншток грозный,';

if($brandname && !empty($_GET['product'])) {
	$title = dinamic_title($_GET['product'], $brandname)['name'];
	$meta_desc = $title;
	$meta_key = $title;
}
else {
	$title = 'МИР ИНСТРУМЕНТА ГРОЗНЫЙ';
}
/*--------------------------------------------------------------------------------*/



?>



<?php require_once "homepage.php" ?>