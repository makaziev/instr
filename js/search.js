$(function(){
	$('.search_form').bind('change keyup input click', function(){
		if (this.value.length >= 2) {
			$.ajax({
				type: 'post',
				url: "../search.php",
				data: {'poisk': this.value},
				// response: 'text',
				success: function(data){
					$('.search_res').html(data);
				}
			})
		}
		else {
			$('.search_res').html('');
		}
	})

	$('*').not('.search_form').on('click', function() {
		$('.search_res').html('');
	});
})