$(function() {
	//сворачивание, разворачивание меню
	$('.menu > span, .menu > span > i').on('click', function() {
		if($(this).parent('.menu').next().css('display') == 'none') {
			$(this).parent('.menu').next().css('display', 'block');
			$(this).parent('.menu').css('border-bottom-color', '#EAE8EA');
			$(this).children('i').attr('class', 'fas fa-minus');
		}
		else {
			$(this).parent('.menu').next().toggle();
			$(this).children('i').attr('class', 'fas fa-plus');
			$(this).parent('.menu').css('border-bottom-color', 'silver');
		}
	})

	$('.submenu2 > span, .submenu2 > span > i').on('click', function() {
		if($(this).parent('.submenu2').next().css('display') == 'none') {
			$(this).parent('.submenu2').next().css('display', 'block');
			$(this).children('i').attr('class', 'fas fa-minus');
		}
		else {
			$(this).parent('.submenu2').next().toggle();
			$(this).children('i').attr('class', 'fas fa-plus');
		}
	})

	//burger
	$('.burger').on('click', function() {
		$('.nav').toggle();
	})

	$('.hide_menu a:eq(0)').on('click', function(event) {
		event.preventDefault();
		$('.hide_menu + div:eq(0)').toggle();
	})

	$('.hide_menu a:eq(1)').on('click', function(event) {
		event.preventDefault();
		$('.hide_menu + div:eq(1)').toggle();
	})

	//удаление лишних плюсиков
	var li = $('.subsubmenu > ul');
	for(let i = 0; i < li.length; i++) {
		if($(li[i]).find('li').length < 1) {
			let sib = $(li[i]).parent().siblings();
			$(sib).find('span').remove();
			// console.log(sib);
		}
		//console.log($(li[i]).find('li'));
	}

	//переключение фотографий
	$('.all_imgs > a').on('click', function(event) {
		event.preventDefault();
		var nameImg = $(this).attr('href');
		$('.img_in > img').attr('src', nameImg);
		$('.img > a').attr('href', nameImg);
	})

	//просмотр фотографии и последующее перелистывание
	$('.img > a').on('click', function(event) {
		event.preventDefault();
		$('main').css('filter', 'blur(2px)');
		var nameImg = $(this).attr('href');
		$('body').css('position', 'relative').css('overflow', 'hidden');
		$('body').
		prepend('<div class="showimg"><div><img src="' + nameImg + '" alt="" /></div></div>');
		var attr = $('.all_imgs > a').toArray();
		var imgCounts = attr.length;
		if(imgCounts > 1)
			$('.showimg div').
			prepend('<button class=\'prevbutton\'><i class="fas fa-angle-left"></i></button><button class=\'nextbutton\'><i class="fas fa-angle-right"></i></button>');
		var fullImg = 'http://mirinstrum/' + nameImg;
		for(let i = 0; i < attr.length; i++) {
			if($(attr[i]).attr('href') == nameImg)
				var imgIndex = i;
		}
		var i = imgIndex;
		$('.nextbutton, .showimg div img').on('click', function() {
			i++;
			if(i > (imgCounts - 1))
				i = 0;
			$('.showimg div img').attr('src', attr[i]);
		})
		$('.prevbutton').on('click', function() {
			i--;
			if(i < 0)
				i = (imgCounts - 1);
			$('.showimg div img').attr('src', attr[i]);
		})
		$('.showimg div').on('click', function(e) {
			if(e.target !== this)
				return;
			$('.showimg').remove();
			$('main').css('filter', '');
			$('body').css('position', '').css('overflow', '');
		})
	})

})