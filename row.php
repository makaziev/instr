<!-- Содержимое класса row_4 -->


<?php while($products = $get_products->fetch_assoc()) : ?>
	<div class="prod">
		<div class="product">
			<div class="product_2">
				<p class="arial">
					Артикул: 
					<b><?php echo $products['vendor_code'] ?></b>
				</p>
				<?php $img_unser = unserialize($products['pictures'])[0] ?>
				<?php
				$vendor = change_vendor_name($products['vendor']);
				$img = "$vendor/$products[category_id]/$img_unser";
				?>
				<div class="image_wrap">
					<a href="?brand=<?php echo $vendor ?>&product=<?php echo $products['vendor_code'] ?>">
						<div>
							<img src="images/images-min/<?php echo $img ?>.jpg" alt="">
						</div>
					</a>
				</div>
				<div class="row-descrip arial">
					<a href="?brand=<?php echo $vendor ?>&product=<?php echo $products['vendor_code'] ?>">
						<?php echo $products['name'] ?>
					</a>
				</div>
				<div class="price">
					<span>Цена: 
						<b>
							<?php echo $products['price'] ?>р
						</b>
					</span>
				</div>
			</div>
		</div>
	</div>
<?php endwhile; ?>

<br clear="both">

<div class="pagination">
	<p>
		<?php if($_GET['category']) : ?>
			<a href='<?php echo "?page=$prev_n&category=$_GET[category]" ?>'>
				<?php echo $prev ?>
			</a>
			<?php
			pagination($pageno, $count_of_data, $_GET['category']);
			?>
			<a href='<?php echo "?page=$next_n&category=$_GET[category]" ?>'>
				<?php echo $next ?>
			</a>
		<?php endif ?>
	</p>
</div>