<?php

function main_menu() {
	global $mysqli;
	$main_menu_sql = "SELECT * FROM `categories` WHERE `parent_id` = false";
	return $mysqli->query($main_menu_sql);
}

function main_menu_sub($cat) {
	global $mysqli;
	$sub_menu_sql = "SELECT * FROM `categories` WHERE `parent_id` = $cat";
	return $mysqli->query($sub_menu_sql);
}

function prof_categories_menu() {
	global $mysqli;
	$prof_categories_menu = "SELECT * FROM `prof_categories` WHERE `parent_id` = false";
	return $mysqli->query($prof_categories_menu);
}

function prof_categories_sub($cat) {
	global $mysqli;
	$prof_menu_sql = "SELECT * FROM `prof_categories` WHERE `parent_id` = $cat";
	return $mysqli->query($prof_menu_sql);
}

//пока не используется
function subcat_count($subcat_id) {
	global $mysqli;
	$subcat_count_sql = "SELECT SUM(T.cnt) FROM (
		SELECT COUNT(id) as cnt FROM `bars` WHERE `old_id` = $subcat_id UNION ALL
		SELECT COUNT(id) as cnt FROM `denzel` WHERE `old_id` = $subcat_id UNION ALL
		SELECT COUNT(id) as cnt FROM `elfe` WHERE `old_id` = $subcat_id UNION ALL
		SELECT COUNT(id) as cnt FROM `gross` WHERE `old_id` = $subcat_id UNION ALL
		SELECT COUNT(id) as cnt FROM `kronwerk` WHERE `old_id` = $subcat_id UNION ALL
		SELECT COUNT(id) as cnt FROM `matrix` WHERE `old_id` = $subcat_id UNION ALL
		SELECT COUNT(id) as cnt FROM `palisad` WHERE `old_id` = $subcat_id UNION ALL
		SELECT COUNT(id) as cnt FROM `sibrtech` WHERE `old_id` = $subcat_id UNION ALL
		SELECT COUNT(id) as cnt FROM `sparta` WHERE `old_id` = $subcat_id UNION ALL
		SELECT COUNT(id) as cnt FROM `stels` WHERE `old_id` = $subcat_id UNION ALL
		SELECT COUNT(id) as cnt FROM `stern` WHERE `old_id` = $subcat_id
	) T";
	return $mysqli->query($subcat_count_sql);
}

function products_uniq_list() {
	global $mysqli;
	$products_uniq_sql = "
	SELECT * FROM `bars` GROUP BY `category_id` UNION
	SELECT * FROM `denzel` GROUP BY `category_id` UNION
	SELECT * FROM `elfe` GROUP BY `category_id` UNION
	SELECT * FROM `gross` GROUP BY `category_id` UNION
	SELECT * FROM `kronwerk` GROUP BY `category_id` UNION
	SELECT * FROM `matrix` GROUP BY `category_id` UNION
	SELECT * FROM `palisad` GROUP BY `category_id` UNION
	SELECT * FROM `sibrtech` GROUP BY `category_id` UNION
	SELECT * FROM `sparta` GROUP BY `category_id` UNION
	SELECT * FROM `stels` GROUP BY `category_id` UNION
	SELECT * FROM `stern` GROUP BY `category_id` ORDER BY RAND() LIMIT 48";
	return $mysqli->query($products_uniq_sql);
}

function products_uniq_prof() {
	global $mysqli;
	$products_uniq_sql = "
	SELECT * FROM `eibenstock` GROUP BY `id_category` UNION
	SELECT * FROM `husqvarna` GROUP BY `id_category` UNION
	SELECT * FROM `knipex` GROUP BY `id_category` UNION
	SELECT * FROM `metabo` GROUP BY `id_category` UNION
	SELECT * FROM `rubi` GROUP BY `id_category` ORDER BY RAND() LIMIT 48";
	return $mysqli->query($products_uniq_sql);
}

// получение списка товаров по категориям
function products_by_category($category_id, $first_d, $second_d) {
	global $mysqli;
	$products_by_category_sql = "
		SELECT * FROM `bars` WHERE `older_id` = $category_id OR `old_id` = $category_id OR `category_id` = $category_id UNION
		SELECT * FROM `denzel` WHERE `older_id` = $category_id OR `old_id` = $category_id OR `category_id` = $category_id UNION
		SELECT * FROM `elfe` WHERE `older_id` = $category_id OR `old_id` = $category_id OR `category_id` = $category_id UNION
		SELECT * FROM `gross` WHERE `older_id` = $category_id OR `old_id` = $category_id OR `category_id` = $category_id UNION
		SELECT * FROM `kronwerk` WHERE `older_id` = $category_id OR `old_id` = $category_id OR `category_id` = $category_id UNION
		SELECT * FROM `matrix` WHERE `older_id` = $category_id OR `old_id` = $category_id OR `category_id` = $category_id UNION
		SELECT * FROM `palisad` WHERE `older_id` = $category_id OR `old_id` = $category_id OR `category_id` = $category_id UNION
		SELECT * FROM `sibrtech` WHERE `older_id` = $category_id OR `old_id` = $category_id OR `category_id` = $category_id UNION
		SELECT * FROM `sparta` WHERE `older_id` = $category_id OR `old_id` = $category_id OR `category_id` = $category_id UNION
		SELECT * FROM `stels` WHERE `older_id` = $category_id OR `old_id` = $category_id OR `category_id` = $category_id UNION
		SELECT * FROM `stern` WHERE `older_id` = $category_id OR `old_id` = $category_id OR `category_id` = $category_id LIMIT $first_d, $second_d";
	return $mysqli->query($products_by_category_sql);
}

// получает список товаров по категориям
function products_by_category_prof($category, $brandname, $first_d, $second_d) {
	global $mysqli;
	if(is_array($category)) {
		$category = implode(',', $category);
		// return $category;
	}
	$prof_products_sql = "SELECT * FROM `$brandname` WHERE `id_category` IN ($category) ORDER BY `id_category` ASC LIMIT $first_d, $second_d";
	return $mysqli->query($prof_products_sql);
}

// получает массив потомок 
function get_menu_tree($category) {
	global $mysqli;
	$arr_tree = [];
	$menu_tree_sql = "SELECT `id` FROM `prof_categories` WHERE `id` = $category OR `parent_id` = $category OR `parent_id` IN (SELECT `id` FROM `prof_categories` WHERE `parent_id` = $category)";
	$an_var = $mysqli->query($menu_tree_sql);
	while($val = $an_var->fetch_assoc()) {
		$arr_tree[] = $val['id'];
	}
	return $arr_tree;
}

// получает один товар по $brand и $artikul
function get_product($brand, $artikul) {
	global $mysqli;
	$get_product_sql = "SELECT * FROM $brand WHERE `vendor_code` = '$artikul'";
	return $mysqli->query($get_product_sql);
}

function get_product_prof($brandname, $artikul) {
	$brandname = strtolower($brandname);
	global $mysqli;
	$get_product_sql = "SELECT * FROM $brandname WHERE `artikul` = '$artikul'";
	return $mysqli->query($get_product_sql);
}

// Используется для пагинации
function count_of_data($category_id) {
	global $mysqli;
	$get_count_of_data = "SELECT SUM(T.cnt) FROM (
		SELECT COUNT(id) as cnt FROM `bars` WHERE `older_id` = $category_id OR `old_id` = $category_id OR `category_id` = $category_id UNION ALL
		SELECT COUNT(id) as cnt FROM `denzel` WHERE `older_id` = $category_id OR `old_id` = $category_id OR `category_id` = $category_id UNION ALL
		SELECT COUNT(id) as cnt FROM `elfe` WHERE `older_id` = $category_id OR `old_id` = $category_id OR `category_id` = $category_id UNION ALL
		SELECT COUNT(id) as cnt FROM `gross` WHERE `older_id` = $category_id OR `old_id` = $category_id OR `category_id` = $category_id UNION ALL
		SELECT COUNT(id) as cnt FROM `kronwerk` WHERE `older_id` = $category_id OR `old_id` = $category_id OR `category_id` = $category_id UNION ALL
		SELECT COUNT(id) as cnt FROM `matrix` WHERE `older_id` = $category_id OR `old_id` = $category_id OR `category_id` = $category_id UNION ALL
		SELECT COUNT(id) as cnt FROM `palisad` WHERE `older_id` = $category_id OR `old_id` = $category_id OR `category_id` = $category_id UNION ALL
		SELECT COUNT(id) as cnt FROM `sibrtech` WHERE `older_id` = $category_id OR `old_id` = $category_id OR `category_id` = $category_id UNION ALL
		SELECT COUNT(id) as cnt FROM `sparta` WHERE `older_id` = $category_id OR `old_id` = $category_id OR `category_id` = $category_id UNION ALL
		SELECT COUNT(id) as cnt FROM `stels` WHERE `older_id` = $category_id OR `old_id` = $category_id OR `category_id` = $category_id UNION ALL
		SELECT COUNT(id) as cnt FROM `stern` WHERE `older_id` = $category_id OR `old_id` = $category_id OR `category_id` = $category_id
	) T";
	return $mysqli->query($get_count_of_data);
}

function count_of_data_prof($category, $brandname) {
	global $mysqli;
	if(is_array($category)) {
		$category = implode(',', $category);
		// return $category;
	}
	$get_count_of_data = "SELECT COUNT(id) FROM $brandname WHERE `id_category` IN ($category)";
	return $mysqli->query($get_count_of_data);
}

function pagination($pageno, $count_of_data, $cat_id, $brandname = '') {
	// echo $pageno . '<br>';
	// echo $count_of_data . '<br>';
	// echo $cat_id . '<br>';
	// echo $brandname . '<br>';
	$prof = '';
	if($brandname) {
		$prof = "&brandname=$brandname&prof=1";
	}

	$dots = '...';
	$one = 1;
	if ($pageno > 3 && $count_of_data >= 4) {
		echo "<span><a class='$css_class' href='?page=$one&category=$cat_id" . $prof . "'>$one</a></span>";
	}
	if ($pageno > 3 && $count_of_data >= 5) {
		echo $dots;
	}
	if ($pageno < 4) {
		for ($i = 1; $i <= 4; $i++) {
			$css_class = '';
			if ($pageno == $i) {
				$css_class = "alink";
			}
			if ($i >= $count_of_data) {
				break;
			}
			echo "<span><a class='$css_class' href='?page=$i&category=$cat_id" . $prof . "'>$i</a></span>";
		}
	}
	elseif ($pageno >= 4 && $pageno <= $count_of_data) {//вот тут тоже поправить
		$start = -1 + $pageno;
		$end = 1 + $pageno;
		if ($pageno >= -1 + $count_of_data) {
			$start = -2 + $count_of_data;
			$end = $count_of_data;
		}
		for ($i = $start; $i <= $end; $i++) {
			$css_class = "";
			if ($pageno == $i) {
				$css_class = "alink";
			}
			if($count_of_data == $i) {
				break;
			}
			echo "<span><a class='$css_class' href='?page=$i&category=$cat_id" . $prof . "'>$i</a></span>";
		}
	}
	$last_dots = $dots;
	if ($pageno == $count_of_data || $count_of_data <= 7 || ($count_of_data - $pageno) <= 2 && $pageno > 5 || $pageno >= 7 && ($count_of_data - $pageno) <= 2) {
		$last_dots = "";
	}
	echo $last_dots;
	if($count_of_data != 1) {
		echo "<span><a class='$css_class' href='?page=$count_of_data&category=$cat_id" . $prof . "'>$count_of_data</a></span>";
	}
}

function change_vendor_name($vendor) {
	$vendor = mb_strtolower($vendor);
	if($vendor == 'mtx') {
		$result = "matrix";
	}
	elseif($vendor == 'сибртех' || $vendor == 'russia') {
		$result = 'sibrtech';
	}
	elseif($vendor == 'барс') {
		$result = 'bars';
	}
	else {
		$result = $vendor;
	}
	return $result;
}

function icons_path($name) {
	if(file_exists("images/icons/" . $name . ".jpg")) {
		$cat_icon = "images/icons/" . $name . ".jpg";
	}
	else {
		$cat_icon = "images/icons/" . $name . ".png";
	}
	return $cat_icon;
}

function dinamic_title($artikul, $brandname) {
	global $mysqli;
	$sql = "SELECT `name` FROM $brandname WHERE `artikul` = '$artikul'";
	return $mysqli->query($sql)->fetch_assoc();
}

function counter() {
	global $mysqli;

	$ip = $_SERVER['REMOTE_ADDR'];
	$user_agent = $_SERVER['HTTP_USER_AGENT'];
	$referer = $_SERVER['HTTP_REFERER'];
	$date = date('Y.m.d H:i:s');

	$result = $mysqli->query("SELECT * FROM visits WHERE `ip` = '$ip'");

	if ($result->fetch_row()) {
		return $mysqli->query("UPDATE visits SET `referer` = '$referer', `count` = `count` + 1, `date` = '$date' WHERE `ip` = '$ip'");
	} else {
		return $mysqli->query("INSERT INTO visits VALUES(null, '$ip', '$referer', '$user_agent', 1, '$date')");
	}
}

function search($query) {
	global $mysqli;

	$query = $mysqli->real_escape_string(trim(strip_tags(stripcslashes(htmlspecialchars($query)))));
	$search_sql = "
		SELECT id, name, vendor, artikul, images, id_category FROM `metabo` WHERE `artikul` LIKE '%$query%' OR `name` LIKE '%$query%' UNION
		SELECT id, name, vendor, artikul, images, id_category FROM `eibenstock` WHERE `artikul` LIKE '%$query%' OR `name` LIKE '%$query%' UNION
		SELECT id, name, vendor, artikul, images, id_category FROM `husqvarna` WHERE `artikul` LIKE '%$query%' OR `name` LIKE '%$query%' UNION
		SELECT id, name, vendor, artikul, images, id_category FROM `knipex` WHERE `artikul` LIKE '%$query%' OR `name` LIKE '%$query%' UNION
		SELECT id, name, vendor, artikul, images, id_category FROM `rubi` WHERE `artikul` LIKE '%$query%' OR `name` LIKE '%$query%' UNION
		SELECT id, category_id, pictures, name, vendor, vendor_code FROM `bars` WHERE `vendor_code` LIKE '%$query%' OR `name` LIKE '%$query%' UNION
		SELECT id, category_id, pictures, name, vendor, vendor_code FROM `denzel` WHERE `vendor_code` LIKE '%$query%' OR `name` LIKE '%$query%' UNION
		SELECT id, category_id, pictures, name, vendor, vendor_code FROM `elfe` WHERE `vendor_code` LIKE '%$query%' OR `name` LIKE '%$query%' UNION
		SELECT id, category_id, pictures, name, vendor, vendor_code FROM `gross` WHERE `vendor_code` LIKE '%$query%' OR `name` LIKE '%$query%' UNION
		SELECT id, category_id, pictures, name, vendor, vendor_code FROM `kronwerk` WHERE `vendor_code` LIKE '%$query%' OR `name` LIKE '%$query%' UNION
		SELECT id, category_id, pictures, name, vendor, vendor_code FROM `matrix` WHERE `vendor_code` LIKE '%$query%' OR `name` LIKE '%$query%' UNION
		SELECT id, category_id, pictures, name, vendor, vendor_code FROM `palisad` WHERE `vendor_code` LIKE '%$query%' OR `name` LIKE '%$query%' UNION
		SELECT id, category_id, pictures, name, vendor, vendor_code FROM `sibrtech` WHERE `vendor_code` LIKE '%$query%' OR `name` LIKE '%$query%' UNION
		SELECT id, category_id, pictures, name, vendor, vendor_code FROM `sparta` WHERE `vendor_code` LIKE '%$query%' OR `name` LIKE '%$query%' UNION
		SELECT id, category_id, pictures, name, vendor, vendor_code FROM `stels` WHERE `vendor_code` LIKE '%$query%' OR `name` LIKE '%$query%' UNION
		SELECT id, category_id, pictures, name, vendor, vendor_code FROM `stern` WHERE `vendor_code` LIKE '%$query%' OR `name` LIKE '%$query%' LIMIT 50
	";

	return $mysqli->query($search_sql);
}

?>