<?php

require_once 'config/init.php';

$data = $_POST['poisk'];

$search_data = search($data);

?>
<?php $prof_vendors = ['metabo', 'eibenstock', 'husqvarna', 'knipex', 'rubi'] ?>

<?php while ($result = $search_data->fetch_assoc()) : ?>

	<li>
		<?php if (in_array(strtolower($result['vendor']), $prof_vendors)) : ?>
			<?php $img = unserialize($result['images']) ?>
			<a href="<?php echo "?brandname=$result[vendor]&product=$result[artikul]&prof=1" ?>">
				<div class="item-img">
					<img src="images/images-min/<?php echo "$result[vendor]/$img[0].jpg" ?>" alt="">
				</div>
				<div class="item-content">
					<?php echo $result['name'] ?>
				</div>
			</a>
		<?php else : ?> 
			<!-- в этом блоке имена ключей массива не соответствуют именам столбцов в бд -->
			<?php $vendor = change_vendor_name($result['images']) ?>
			<?php $img = unserialize($result['vendor']) ?>
			<?php //var_dump($result) ?>
			<a href="<?php echo "?brand=$result[images]&product=$result[id_category]" ?>">
				<div class="item-img">
					<img src="images/images-min/<?php echo "$vendor/$result[name]/$img[0].jpg" ?>" alt="">
				</div>
				<div class="item-content">
					<?php echo $result['artikul'] ?>
				</div>
			</a>
		<?php endif ?>
	</li>

<?php endwhile ?>