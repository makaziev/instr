<div class="product_page">
	<article class="article">
		<h1 class="product_name"><?php echo $get_product_result['name'] ?></h1>
		<div class="two_blocks">
			<div class="img">
				<!-- <div class="brand">
					<?php $change_vendor = change_vendor_name($get_product_result['artikul']) ?>
					<a href="?vendor=<?php echo $change_vendor ?>">
						<img src="images/brand/<?php echo $change_vendor ?>.png" alt="">
					</a>
				</div> -->
				<?php //if($brandname == 'knipex') : ?>
					<?php //$pictures[0][] = $get_product_result['images'] ?>
					<?php //$img = "images/$brandname/" . $pictures[0][0] . ".jpg" ?>
				<?php //else : ?>
					<?php $pictures[] = unserialize($get_product_result['images']) ?>
					<?php $img = "images/$brandname/" . $pictures[0][0] . ".jpg" ?>
				<?php //endif; ?>
				<a href="<?php echo $img ?>">
					<div class="img_in">
						<img src="<?php echo $img ?>" alt="">
					</div>
				</a>
				<div class="all_imgs">
					<?php foreach($pictures[0] as $value) : ?>
						<?php $imgs = "images/$brandname/$value.jpg" ?>
						<a href="<?php echo $imgs ?>">
							<div class="img_min">
								<img src="<?php echo $imgs ?>" alt="">
							</div>
						</a>
					<?php endforeach; ?>
				</div>
			</div>
			<div class="params">
				<div class="vendor_code">
					Артикул: <span><?php echo $get_product_result['artikul'] ?></span>
				</div>
				<?php $params = unserialize($get_product_result['params']) ?>
				<ul>
					<li>Бренд:
						<a href="?vendor=<?php echo $brandname ?>">
							<?php echo strtoupper($brandname) ?>
						</a>
					</li>
					<?php $i = 0; ?>
					<?php if($params) : ?>
						<?php foreach($params as $param) : ?>
							<?php echo $i % 2 == 0 ? "<li>$param" : "<span><b>$param</b></span></li>";?>
							<?php $i++ ?>
						<?php endforeach; ?>
					<?php endif; ?>
				</ul>
				<div class="cost">
					<p>Цена: 
						<span><?php echo $get_product_result['price'] ?>р</span>
					</p>
				</div>
				<hr>
			</div>
		</div>
		<div class="description">
			<div class="desc_icon">
				<i class="fas fa-list"></i>
				ОПИСАНИЕ
			</div>
			<?php $descriptions = unserialize($get_product_result['description']) ?>
			<?php if($brandname == 'eibenstock'): ?>
				<?php echo $descriptions ?>
			<?php else: ?>
				<?php if($descriptions) : ?>
					<?php foreach($descriptions as $value) : ?>
						<p><!-- <i class="fas fa-angle-right"></i> --><?php echo $value ?></p>
					<?php endforeach; ?>
				<?php endif; ?>
			<?php endif; ?>
		</div>
	</article>
</div>