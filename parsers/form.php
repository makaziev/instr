<?php require_once '../config/init.php'; ?>

<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<script src="../js/jquery-3.3.1.js"></script>
	<title>Форма для парсинга профессиональных инструментов</title>
	<script type="text/javascript">
		$(function() {
			$('.menu1').on('change', function(event) {
				event.preventDefault();
				let op = $(this).val();
				$.post('form_action.php', {cat: op},
					function(data) {
						$('.menu2, .menu3, .br').remove();
						$('.br1').after(data);
					}
				);
			});
			$(document).on('change', '.menu2', function(event) {
				event.preventDefault();
				let op = $(this).val();
				$.post('form_action.php', {cat_sub: op},
					function(data) {
						$('.menu3, .br').remove();
						$('input.field, .br2').remove();
						$('.menu2').after(data);
					}
				);
			});
			$(document).on('change', '.menu3', function(event) {
				event.preventDefault();
				let op = $(this).val();
				$.post('form_action.php', {cat_sub_sub: op},
					function(data) {
						$('input.field, .br2').remove();
						$('.menu3').after(data);
					}
				);
			});
			$('form').submit(function(event) {
				event.preventDefault();
				$('.print').remove();
				let op = $('input.field', this).val();
				let third = $('.menu3').val();
				let second = $('.menu2').val();
				let brand = $('.menu1 option:selected').text();
				$.post('form_action.php',
					{form_data: op, cat3: third, cat2: second, brand: brand},
					function(data) {
						$('span').remove();
						$('input.field').val('');
						$('input.field').after(data);
					}
				);
			});
		});
	</script>
	<style>
		body {
			margin-top: 50px;
		}
		.main {
			width: 70%;
			background-color: #fff;
			margin: 0 auto;
			font-family: Arial;
		}
		.main select {
			padding: 5px;
			border-color: #E5E5E5;
			font-size: 18px;
			color: #6F6F6F;
			outline: none;
		}
		.main input[type='submit'] {
			padding: 8px 10px;
			background-color: #0080FF;
			border: none;
			outline: none;
			color: #fff;
		}
		.main input[type='submit']:hover {
			cursor: pointer;
		}
		.field {
			width: 100%;
			padding: 10px;
			border-radius: 5px;
			border: 1px solid #ccc;
			outline: none;
			font-size: 16px;
		}
	</style>
</head>
<body>
	<div class="main">
		<form action="" method="POST" id="option">
			<select class="menu1" name="menu1" form="option">
				<option value=""></option>
				<?php $menu = prof_categories_menu() ?>
				<?php while($item = $menu->fetch_assoc()) : ?>
					<option value="<?php echo $item[id] ?>"><?php echo $item['name'] ?></option>
				<?php endwhile; ?>
			</select><br class="br1">
			<br><br><br>
			<input type="submit" value="Сохранить в базу">
		</form>
	</div>
</body>
</html>