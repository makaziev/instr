<?php require_once 'header.php' ?>

<body>

	<!-- <div class="topnav">
		<div class="burger">
			<i class="fas fa-bars"></i>
			<span>Меню</span>
		</div>
		<nav class="nav">
			<div>
				<ul>
					<li>
						<a href="#"><i class="fas fa-info-circle"></i>О компании</a>
					</li>
					<li>
						<a href="#"><i class="fas fa-map-marker-alt"></i>Адрес</a>
					</li>
					<li>
						<a href="#"><i class="fas fa-award"></i>Бренды</a>
					</li>
					<li>
						<a href="#"><i class="fas fa-percent"></i>Акции</a>
					</li>
					<li>
						<a href="#"><i class="far fa-newspaper"></i>Новости</a>
					</li>
					<li>
						<a href="#"><i class="fas fa-address-book"></i>Контакты</a>
					</li>
					<li>
						<a href="#"><i class="far fa-comment-dots"></i>Обратная связь</a>
					</li>
				</ul>
			</div>
		</nav>
	</div> -->

	<header>
		<div>
			<div class="logo">
				<a href="/">
					<img src="images/logo.jpg" alt="">
				</a>
			</div>
			<div class="cont">
				<p class="phone">
					<a href="tel:89389977755">
						<i class="fas fa-phone-alt"></i>
						8 (938) 997-77-55
					</a>
				</p>
				<p class="mail" style="letter-spacing: 1px;">
					<a href="mailto:d_yates@mail.ru">
						<i class="fas fa-envelope"></i>
						mirinstrumenta95@mail.ru
					</a>
				</p>
				<p class="shed">
					<a href="https://www.google.ru/maps/@43.3272041,45.7031185,19z?hl=ru">
						<i class="fas fa-map-marker-alt"></i>
						г. Грозный, ул. Хизира Кишиева, 17
					</a>
				</p>
			</div>
			<div class="search">
				<form action="#">
					<input class="search_form" name="poisk" placeholder="Поиск" type="search" autocomplete="off">
					<button><i class="fas fa-search"></i></button>
					<ul class="search_res"></ul>
				</form>
			</div>
		</div>
	</header>

	<main class="main">
		<div>
			<div class="main_2">
				<aside class="aside">
					<div class="hide_menu">
						<a href="#">
							<i class="fas fa-bars"></i>
							Профессиональный инструмент
						</a>
					</div>
					<div>
						<ul>
							<?php $y = 0 ?>
							<?php while($prof_cat = $prof_categories_menu_result->fetch_assoc()): ?>
								<li>
									<div class="menu">
										<a style="line-height: 40px; padding-left: 5px;" href="?category=<?php echo $prof_cat['id'] ?>&brandname=<?php echo $prof_cat['name'] ?>&prof=1">
											<img src="images/<?php echo strtolower($prof_cat['name']) ?>.jpg" alt="">
											<?php echo $prof_cat['name'] ?>
											<?php $y++ ?>
										</a>
										<span>
											<i class="fas fa-plus"></i>
										</span>
									</div>
									<div class="submenu">
										<ul>
											<?php $prof_menu_result = prof_categories_sub($prof_cat['id']); ?>
											<?php while($prof_cat_sub = $prof_menu_result->fetch_assoc()): ?>
												<li style="padding-left: 30px;">
													<div class="submenu2">
														<a href="?category=<?php echo $prof_cat_sub['id'] ?>&brandname=<?php echo $prof_cat['name'] ?>&prof=1">
															<?php echo $prof_cat_sub['name'] ?>
														</a>
														<span>
															<i class="fas fa-plus"></i>
														</span>
													</div>
													<div class="subsubmenu">
														<ul>
															<?php $prof_sub_sub = prof_categories_sub($prof_cat_sub['id']); ?>
															<?php while($sub_sub_prof = $prof_sub_sub->fetch_assoc()) : ?>
																<li>
																	<div>
																		<a href="?category=<?php echo $sub_sub_prof['id'] ?>&brandname=<?php echo $prof_cat['name'] ?>&prof=1">
																			<img src="<?php echo $cat_icon ?>" alt="">
																			<?php echo $sub_sub_prof['name'] ?>
																		</a>
																	</div>
																</li>
															<?php endwhile; ?>
														</ul>
													</div>
												</li>
											<?php endwhile; ?>
										</ul>
									</div>
								</li>
							<?php endwhile; ?>
						</ul>
					</div>
					<br>
					<div class="hide_menu">
						<a href="#">
							<i class="fas fa-bars"></i>
							Домашний инструмент
						</a>
					</div>
					<div>
						<ul>
							<?php while ($cat = $main_menu_result->fetch_assoc()) : ?>
								<li>
									<div class="menu">
										<a href="?category=<?php echo $cat['id_category'] ?>">
											<img src="images/icons/<?php echo $cat['id_category'] ?>.png" alt="">
											<?php echo $cat['category'] ?>
										</a>
										<span>
											<i class="fas fa-plus"></i>
										</span>
									</div>
									<div class="submenu">
										<ul>
											<?php $sub_menu_result = main_menu_sub($cat['id_category']); ?>
											<?php while ($subcat = $sub_menu_result->fetch_assoc()) : ?>
												<?php
													$cat_icon = icons_path($subcat['id_category']);
												?>
												<li>
													<div class="submenu2">
														<a href="?category=<?php echo $subcat['id_category'] ?>">
															<img src="<?php echo $cat_icon ?>" alt="">
															<?php echo $subcat['category'] ?>
														</a>
														<span>
															<i class="fas fa-plus"></i>
														</span>
													</div>
													<div class="subsubmenu">
														<ul>
															<?php $sub_sub_menu = main_menu_sub($subcat['id_category']); ?>
															<?php while($sub_sub_cat = $sub_sub_menu->fetch_assoc()) : ?>
																<?php $cat_icon = icons_path($sub_sub_cat['id_category']) ?>
																<li>
																	<div>
																		<a href="?category=<?php echo $sub_sub_cat['id_category'] ?>">
																			<img src="<?php echo $cat_icon ?>" alt="">
																			<?php echo $sub_sub_cat['category'] ?>
																		</a>
																	</div>
																</li>
															<?php endwhile; ?>
														</ul>
													</div>
												</li>
											<?php endwhile; ?>
										</ul>
									</div>
								</li>
							<?php endwhile; ?>
						</ul>
					</div>
				</aside>

				<div class="row">
					<div>
						<div>
							<div class="row_4">
								<?php require_once $row_4 ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</main>

<?php require_once 'footer.php' ?>