<!DOCTYPE html>
<html lang="ru">
<head>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-104328235-3"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-104328235-3');
	</script>

	<title><?php echo $title ?></title>
	<meta charset="UTF-8">
	<meta name="description" content="<?php echo $meta_desc ?>">
	<meta name="Keywords" content="<?php echo $meta_key ?>">
	<meta name="viewport" content="width=device-width,initial-scale=1">
	<meta name="google-site-verification" content="nLi90P6Q78FF314PqcfMFSh9Mn6w6m5-R4rMBrs0b-A" />
	<meta name="yandex-verification" content="bbc0243247d94279" />
	<link rel="icon" href="images/favicon.ico" type="image/x-icon">
	<link rel="stylesheet" href="assets/fontawesome-free-5.8.1-web/css/all.min.css">
	<link rel="stylesheet" href="css/style.css">
	<!-- <meta name="viewport" content="width=device-width"> -->
	
	<!-- Yandex.Metrika counter -->
	<script type="text/javascript" >
	   (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
	   m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
	   (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

	   ym(57306094, "init", {
	        clickmap:true,
	        trackLinks:true,
	        accurateTrackBounce:true,
	        webvisor:true
	   });
	</script>
	<noscript><div><img src="https://mc.yandex.ru/watch/57306094" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
	<!-- /Yandex.Metrika counter -->
	<script data-ad-client="ca-pub-3060386536803514" async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
	<script src="js/jquery-3.3.1.js"></script>
	<script src="js/script.js"></script>
	<script src="js/search.js"></script>
</head>