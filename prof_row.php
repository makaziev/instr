<!-- Содержимое класса row_4 -->


<?php while($products = $get_products->fetch_assoc()) : ?>
	<div class="prod">
		<div class="product">
			<div class="product_2">
				<p class="arial">
					Артикул: 
					<b><?php echo $products['artikul'] ?></b>
				</p>
				<?php //if($brandname == 'knipex') : ?>
					<?php //$img_unser = $products['images'] ?>
				<?php //else : ?>
					<?php $img_unser = unserialize($products['images']); ?>
					<?php $img_unser = $img_unser[0] ?>
				<?php //endif; ?>
				<?php
				// $vendor = change_vendor_name($products['vendor']);
				$img = "$products[vendor]/$img_unser";
				?>
				<div class="image_wrap">
					<a href='<?php echo "?brandname=$products[vendor]&product=$products[artikul]&prof=1" ?>'>
						<div>
							<img src="images/images-min/<?php echo $img ?>.jpg" alt="">
						</div>
					</a>
				</div>
				<div class="row-descrip arial">
					<a href='<?php echo "?brandname=$products[vendor]&product=$products[artikul]&prof=1" ?>'>
						<?php echo $products['name'] ?>
					</a>
				</div>
				<div class="price">
					<span>Цена: 
						<b>
							<?php echo $products['price'] ?>р
						</b>
					</span>
				</div>
			</div>
		</div>
	</div>
<?php endwhile; ?>

<br clear="both">

<div class="pagination">
	<p>
		<?php if(!empty($_GET['category'])) : ?>
			<a href='<?php echo "?page=$prev_n&category=$_GET[category]&brandname=$brandname&prof=1" ?>'>
				<?php echo $prev ?>
			</a>
			<?php
			pagination($pageno, $count_of_data, $_GET['category'], $brandname);
			?>
			<a href='<?php echo "?page=$next_n&category=$_GET[category]&brandname=$brandname&prof=1" ?>'>
				<?php echo $next ?>
			</a>
		<?php endif ?>
	</p>
</div>