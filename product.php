<div class="product_page">
	<article class="article">
		<h1 class="product_name"><?php echo $get_product_result['name'] ?></h1>
		<div class="two_blocks">
			<div class="img">
				<div class="brand">
					<?php $change_vendor = change_vendor_name($get_product_result['vendor']) ?>
					<a href="?vendor=<?php echo $change_vendor ?>">
						<img src="images/brand/<?php echo $change_vendor ?>.png" alt="">
					</a>
				</div>
				<?php $pictures[] = unserialize($get_product_result['pictures']) ?>
				<?php $img = "images/$change_vendor/$get_product_result[category_id]/" . $pictures[0][0] . ".jpg" ?>
				<a href="<?php echo $img ?>">
					<div class="img_in">
						<img src="<?php echo $img ?>" alt="">
					</div>
				</a>
				<div class="all_imgs">
					<?php foreach($pictures[0] as $value) : ?>
						<?php $imgs = "images/$change_vendor/$get_product_result[category_id]/$value.jpg" ?>
						<a href="<?php echo $imgs ?>">
							<div class="img_min">
								<img src="<?php echo $imgs ?>" alt="">
							</div>
						</a>
					<?php endforeach; ?>
				</div>
			</div>
			<div class="params">
				<div class="vendor_code">
					Артикул: <span><?php echo $get_product_result['vendor_code'] ?></span>
				</div>
				<?php $params = unserialize($get_product_result['params']) ?>
				<ul>
					<li>Бренд:
						<a href="?vendor=<?php echo $get_product_result['vendor'] ?>">
							<?php echo $get_product_result['vendor'] ?>
						</a>
					</li>
					<?php for($i = 0; $i < count($params[0]); $i++) : ?>
						<li>
							<?php echo $params[0][$i] ?>:
							<span>
								<b><?php echo $params[1][$i] ." ". $params[2][$i] ?></b>
							</span>
						</li>
					<?php endfor; ?>
				</ul>
				<div class="cost">
					<p>Цена: 
						<span><?php echo $get_product_result['price'] ?>р</span>
					</p>
				</div>
				<hr>
			</div>
		</div>
		<div class="description">
			<div class="desc_icon">
				<i class="fas fa-list"></i>
				ОПИСАНИЕ
			</div>
			<?php $descriptions = unserialize($get_product_result['description']) ?>
			<?php foreach($descriptions as $value) : ?>
				<p><i class="fas fa-angle-right"></i><?php echo $value ?></p>
			<?php endforeach; ?>
		</div>
	</article>
</div>